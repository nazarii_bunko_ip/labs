<?php
include "config.php";

// Підключення до бази даних
$conn = new mysqli('localhost', 'root', '', 'students');
// Перевірка з'єднання
if ($conn->connect_error) {
    handle_error(500, "Connection failed: " . $conn->connect_error, 'database');
}
function addStudent($student, $arrGroup, $arrGender) {

    $newRow = '
    <tr data-id="' . $student['id'] . '">
    <th scope="row">
        <input class="form-check-input" type="checkbox" value="">
    </th>
    <td data-value="' . $student['idGroup'] . '">' . $arrGroup[$student['idGroup']] . '</td>
    <td>' . $student['firstName'] . ' ' . $student['secondName'] . '</td>
    <td data-value="' . $student['idGender'] . '">' . $arrGender[$student['idGender']] . '</td>
    <td>' . $student['birthday'] . '</td>
    <td><i class="bi bi-circle-fill circle-grey ' . ($student['status'] ? 'active' : '') . '"></i></td>
    <td>
        <div class="button-container">
            <div class="button-add-edit">
                <i class="bi bi-pencil-square"></i>
            </div>
            <div class="button-delete">
                <i class="bi bi-x-square button-delete"></i>
            </div>
        </div>
    </td>
    </tr>';

    echo $newRow;
}

$sql = "SELECT * FROM students";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $data = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($data as $student) {
        addStudent($student, $arrGroup, $arrGender);
    }
}