const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/111', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('Connected to MongoDB');
}).catch((error) => {
    console.error('MongoDB connection error:', error);
});
const io=require('socket.io')(3000,{
    cors:{
        origin: "http://192.168.50.52",
    },
});

const chatSchema = new mongoose.Schema({
    participants: [String],
    messages: [{
        user: String,
        msg: String,
    }]
});
const userSchema = new mongoose.Schema({
    username: String,
    socket: String
});

const Chat = mongoose.model('Chat', chatSchema);
const User = mongoose.model('User', userSchema);

module.exports = { Chat, User };

Chat.find({ participants: { $all: ["all"] } }).then(chat=>{
    if(chat.length===0){
        const newChat = new Chat({
            participants: ["all"],
            messages: []
        });
        return newChat.save();
    }
})
io.on('connection', (socket) => {
    socket.on('send message', (data, info) => {
        if(info === "all") {
            addMessageToChat("all",socket.username, data, socket, info);

        }else{
            addMessageToChat(info,socket.username, data, socket, info);
        }
    });
    socket.on('get chat',(user, chat)=>{
        if(chat==="all"){
            Chat.findOne({ participants: { $all: ["all"] } })
                .then(findChat => {
                    socket.emit('give chat', findChat, chat);
                })
                .catch(err => {
                    console.error('Помилка при отриманні чатів:', err);
                });
        }else{
            let participants = [chat, user];

            Chat.findOne({ participants: { $all: participants, $size: participants.length } })
                .then(findChat => {
                    socket.emit('give chat', findChat,chat);
                })
                .catch(err => {
                    console.error('Помилка при отриманні чатів:', err);
                });
        }

    })
    socket.on("new user",(data)=>{
        socket.username = data;
        User.findOne({ username: data })
            .then(existingUser => {
                if (!existingUser) {
                    const newUser = new User({
                        username: data,
                        socket: socket.id
                    });

                    return newUser.save();
                } else {
                    existingUser.socket = socket.id;
                    return existingUser.save();
                }
            })
            .then(savedUser => {
                getUsers();
            })
            .catch(err => {
                console.error(`Помилка при збереженні користувача ${data} до бази даних:`, err);
            });
    })
});

function getUsers(){
    User.find({}, 'username')
        .then(users => {
            const usernames = users.map(user => user.username);
            io.sockets.emit('get user', usernames);
        })
        .catch(err => {
            console.error('Помилка при отриманні користувачів з бази даних:', err);
        });
}
function addMessageToChat(chat, user, msg, socket, info){
    let participants;
    if(chat==='all'){
        participants = ['all'];
    }else{
        participants = [chat, user];
    }

    Chat.findOne({ participants: { $all: participants, $size: participants.length } })
        .then(chat => {
            if (!chat) {
                const newChat = new Chat({
                    participants: participants,
                    messages: [{ user: user, msg: msg }]
                });
                return newChat.save();
            } else {
                const newMessage = { user: user, msg: msg };
                chat.messages.push(newMessage);
                return chat.save();
            }
        })
        .then(savedChat => {
            if(savedChat.participants[0]==='all'){
                io.sockets.emit('new message', {msg: msg, user: socket.username, chat:info});
            }else {
                getSocketByUsername(info)
                    .then(socketId => {
                        let sockett = getSocketById(socketId);
                        if (sockett) {
                            sockett.emit('new message',{msg: msg, user: socket.username, chat:socket.username})
                        }
                    });
                socket.emit('new message',{msg: msg, user: socket.username, chat:info});
            }
        })
        .catch(err => {
            console.error('Помилка при створенні або оновленні чату:', err);
        });

}

async function getSocketByUsername(username) {
    try {
        const user = await User.findOne({ username: username });
        if (user) {
            return user.socket;
        } else {
            return null;
        }
    } catch (error) {
        console.error('Помилка при пошуку користувача за іменем:', error);
        return null;
    }
}
function getSocketById(socketId) {
    const activeConnections = io.sockets.sockets;

    let foundSocket = null;

    activeConnections.forEach((socket, currentSocketId) => {
        if (socketId === currentSocketId) {
            foundSocket = socket;
        }
    });

    return foundSocket;
}
