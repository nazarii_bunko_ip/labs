const socket = io('http://192.168.50.52:3000');

const form = document.getElementById('messageForm');
const input = document.getElementById('message');
const chat = document.getElementById('chat');
const userForm = document.getElementById('userForm');
const username = document.getElementById('username');
const chatArea = document.getElementById('chatArea');
const userNameArea = document.getElementById('userNameArea');
const users = document.getElementById('users');
const nameChat = document.getElementById('chatInfo');
const usersInChat = document.getElementById("usersInChat");
let choosenChat = "all";
let user;
const allChats = {};
let usersOnline = [];
let notReadChats = [];
allChats["all"] = [];

form.addEventListener('submit', (e) => {
    e.preventDefault();  // Додано для запобігання перезавантаженню сторінки
    if (input.value) {
        socket.emit('send message', input.value, choosenChat);
        input.value = '';
    }
});

userForm.addEventListener('submit', (e) => {
    e.preventDefault();
    if(username.value){
        socket.emit('new user', username.value);
        user = username.value;
        chatArea.style.display = 'flex';
        userNameArea.style.display = 'none';
        socket.emit('get chat', username.value, choosenChat);
    }
});

socket.on('new message', (data) => {
    if (data.chat === choosenChat){
        socket.emit('get chat', user, choosenChat);
    } else {
        notReadChats.push(data.chat);
    }
    updateUsers(usersOnline);
});

socket.on('get user', (data) => {
    usersOnline = data;
    updateUsers(usersOnline);
    updateOnline(choosenChat);
});

function updateUsers(data) {
    users.innerHTML = "";
    addChat('all');
    data.forEach((name) => {
        if (name !== user) {
            addChat(name);
        }
    });
    chat.scrollTop = chat.scrollHeight;
}

users.addEventListener('click', (e) => {
    if (e.target.classList.contains('mbr')) {
        socket.emit('get chat', username.value, e.target.dataset.name);
    }
});

socket.on('give chat', (chats, name) => {
    choosenChat = name;
    if (choosenChat === "all") {
        nameChat.innerHTML = `Chat: ${getAllUsersString(usersOnline)}`;
    } else {
        nameChat.innerHTML = `Chat: ${choosenChat}`;  // Додано 'Chat: '
    }

    if (notReadChats.includes(choosenChat)) {
        notReadChats.splice(notReadChats.indexOf(choosenChat), 1);
    }
    chat.innerHTML = '';
    if (chats) {
        let messages = chats.messages;
        for (let i = 0; i < messages?.length; i++) {
            if (messages[i].user === user) {
                addMyMessage(messages[i].user, messages[i].msg);
            } else {
                addInMessage(messages[i].user, messages[i].msg);
            }
        }
    }
    chat.scrollTop = chat.scrollHeight;
    updateUsers(usersOnline);
    updateOnline(choosenChat);
});

function getAllUsersString(users) {
    return users.join(', ');
}

function updateOnline(name){
    usersInChat.innerHTML = "";
    if (name === "all") {
        for (let i = 0; i < usersOnline.length; i++) {
            usersInChat.innerHTML += `
                <div class="img m-1 d-flex align-items-center"> <img src="standart.png" alt="">
                                    <div class="nameUser">${usersOnline[i]}</div>
                                </div>
            `;
        }
    } else {
        usersInChat.innerHTML += `
                <div class="img m-1 d-flex align-items-center"> <img src="standart.png" alt=""><div class="nameUser">${user}</div></div>
                <div class="img m-1 d-flex align-items-center"> <img src="standart.png" alt=""><div class="nameUser">${name}</div></div>
            `;
    }
}

function addMyMessage(who, mess){
    let html = `<div class="outgoing_msg" style="display: flex; justify-content: flex-end;">
                    <div class="sent_msg" style="max-width: 60%; background-color: #f1f1f1; border-radius: 10px; padding: 10px; margin: 10px 0; text-align: right;">
                        <p style="margin: 0;">${mess}</p>
                    </div>
                </div>`;
    chat.innerHTML += html;
}

function addInMessage(who, mess){
    let html = `<div class="incoming_msg" style="display: flex; justify-content: flex-start;">
                    <div class="incoming_msg_img" style="display: flex; align-items: center; margin-right: 10px;">
                        <img src="standart.png" alt="" style="width: 30px; height: 30px;">
                    </div>
                    <div class="received_msg" style="max-width: 60%; background-color: #e1e1e1; border-radius: 10px; padding: 10px; margin: 10px 0; text-align: left;">
                        <p style="margin: 0;"><strong>${who}:</strong> ${mess}</p>
                    </div>
                </div>`;
    chat.innerHTML += html;
}

function addChat(name) {
    const isActive = choosenChat === name ? 'active_chat' : '';
    const isUnread = notReadChats.includes(name) ? 'gotten' : '';
    const html = `
        <div class="chat_list mbr ${isActive}" data-name="${name}" style="margin-bottom: 8px;">
            <div class="chat_people mbr" data-name="${name}">
                <div class="chat_img mbr" data-name="${name}">
                    <img src="standart.png" alt="" class="mbr" data-name="${name}">
                </div>
                <div class="chat_ib mbr" data-name="${name}" style="margin-left: 10px; margin-top: -22px;">
                    <h5 class="mbr" data-name="${name}">
                        ${name} <span class="chat_date"><span class="dot ${isUnread}"></span></span>
                    </h5>
                </div>
            </div>
        </div>`;
    users.innerHTML += html;
}