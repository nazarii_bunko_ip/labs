<?php
// Підключення до бази даних
$conn = new mysqli('localhost', 'root', '', 'students');

// Перевірка з'єднання
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Отримання даних студента для видалення з запиту
$data = json_decode(file_get_contents("php://input"), true);
$id = $data['id'];

// Видалення студента з бази даних за його ім'ям та прізвищем
$stmt = $conn->prepare("DELETE FROM students WHERE id = ?");
$stmt->bind_param("i", $id);

$response = array();

if ($stmt->execute()) {
    $response['success'] = true;
    $response['message'] = "Student deleted successfully";
} else {
    $response['success'] = false;
    $response['error'] = "Error deleting student: " . $conn->error;
}

echo json_encode($response);

// Закриття з'єднання з базою даних
$stmt->close();
$conn->close();
?>
