const io = require('socket.io')(3000, {
    cors: {
        origin: "http://192.168.13.248",
    },
});

const users = [];
const connections = [];

io.on('connection', (socket) => {
    connections.push(socket);

    // Обробка відключення користувача
    socket.on('disconnect', () => {
        const userIndex = users.findIndex(user => user.id === socket.id);
        if (userIndex !== -1) {
            users.splice(userIndex, 1);
            io.sockets.emit('get user', users.map(user => user.name));
        }
        connections.splice(connections.indexOf(socket), 1);
    });

    // Обробка відправки повідомлення
    socket.on('send message', (message, chat) => {
        const user = users.find(user => user.id === socket.id);
        if (user) {
            if (chat === "all") {
                io.sockets.emit('new message', { msg: message, user: user.name, chat: "all" });
            } else {
                const recipient = users.find(u => u.name === chat);
                if (recipient) {
                    socket.to(recipient.id).emit('new message', { msg: message, user: user.name, chat: user.name });
                    socket.emit('new message', { msg: message, user: user.name, chat: chat });
                }
            }
        }
    });

    // Обробка нового користувача
    socket.on('new user', (username) => {
        users.push({ id: socket.id, name: username });
        io.sockets.emit('get user', users.map(user => user.name));
    });
});
