<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Messages</title>
    <style>
        .notification {
            background-color: #f8d7da;
            color: #721c24;
            border: 1px solid #f5c6cb;
            border-radius: 5px;
            padding: 10px;
            margin: 5px 0;
        }
        .user-item {
            position: relative;
            padding-right: 20px;
        }
        .user-item .notification-dot {
            position: absolute;
            top: 10px;
            right: 10px;
            width: 10px;
            height: 10px;
            background-color: red;
            border-radius: 50%;
            display: none;
        }
        .user-item.new-message .notification-dot {
            display: inline-block;
        }
    </style>
</head>
<body>
<div class="pos-f-t">
    <div class="offcanvas offcanvas-start bg-dark" tabindex="-1" id="navbarToggleExternalContent" aria-labelledby="navbarToggleExternalContentLabel">
        <div class="offcanvas-header">
            <h5 class="text-white" id="navbarToggleExternalContentLabel">Menu</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link text-white" href="dashboards.html">Dashboards</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="index.php">Students</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="tasks.html">Tasks</a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-dark bg-secondary">
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="d-flex">
            <div class="bell-div">
                <a href="messages.html" class="text-black">
                    <i class="bi bi-bell"></i>
                </a>
                <div class="blink"></div>
            </div>
            <div class="logo-div">
                <img src="Greendenvald.jpg" alt="Logo" class="logo">
                <div class="logo-menu">
                    <a href="#">Profile</a>
                    <a href="#">Log Out</a>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="line">
    <p id="CMS">CMS</p>
    <div class="d-flex bell_logo">
        <div class="bell-div">
            <a href="messages.html" class="text-black">
                <i class="bi bi-bell"></i>
            </a>
            <div class="blink"></div>
            <div class="bell-menu">
                <a>
                    <div class="message-div">
                        <img src="Avatar1.jpg" alt="Avatar" class="message-avatar">
                        <div class="triangle-container">
                            <div class="triangle-border"></div>
                        </div>
                        <div class="message-message">
                            <span>Привіт, як ти?</span>
                        </div>
                    </div>
                </a>
                <a>
                    <div class="message-div">
                        <img src="Avatar2.jpg" alt="Avatar" class="message-avatar">
                        <div class="triangle-container">
                            <div class="triangle-border"></div>
                        </div>
                        <div class="message-message">
                            <span>Здороууу</span>
                        </div>
                    </div>
                </a>
                <a>
                    <div class="message-div">
                        <img src="Avatar3.jpeg" alt="Avatar" class="message-avatar">
                        <div class="triangle-container">
                            <div class="triangle-border"></div>
                        </div>
                        <div class="message-message">
                            <span>Ти зробив ПВІ?</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="logo-div">
            <img src="Greendenvald.jpg" alt="Logo" class="logo">
        </div>
        <div class="fs-2 logo-text">ItsNeMo
            <div class="logo-menu">
                <a href="#">Profile</a>
                <a href="#">Log Out</a>
            </div>
        </div>
    </div>
</div>
<div class="dropdown">
    <a href="dashboards.html" class="list-group-item list-group-item-action border-0 mb-2 mt-3">Dashboard</a>
    <a href="index.php" class="list-group-item list-group-item-action border-0 mb-2">Students</a>
    <a href="tasks.html" class="list-group-item list-group-item-action border-0 mb-2">Tasks</a>
</div>

<div id="String">Messages</div>

<div class="container">
    <div id="userNameArea" style="position: sticky; bottom: 0; background-color: #fff; padding: 10px;">
        <form id="userForm" action="">
            <div class="form-group">
                <label style="margin-right: 10px;">Enter username:</label>
                <input id="username" autocomplete="off" style="padding: 5px; border: 1px solid #ccc; border-radius: 5px; margin-right: 10px;">
                <button id="okButton" style="padding: 5px 10px; border: none; border-radius: 5px; background-color: #007bff; color: #fff; cursor: pointer;">ОК</button>
            </div>
        </form>
    </div>
    <div class="row" id="chatArea" style="display: none;">
        <div class="col-md-4" style="padding: 10px;">
            <h3>Online Users</h3>
            <ul class="list-group" id="users" style="margin-top: 10px;"></ul>
        </div>
        <div class="col-md-8" style="padding: 10px;">
            <h3 id="chatInfo">Chat: all</h3>
            <div class="chat" id="chat" style="border: 1px solid #dee2e6; border-radius: 5px; padding: 10px; margin-top: 10px; height: 500px; overflow-y: auto;"></div>
            <form id="messageForm" class="mt-4 z-0">
                <div class="form-group d-flex align-items-center">
                    <label for="message" class="flex-grow-3" style="width: auto;">Enter message:</label>
                    <input type="text" id="message" class="flex-grow-1 me-2 ms-2" autocomplete="off" style="padding: 5px; border: 1px solid #ccc; border-radius: 5px; margin-right: 10px; width: calc(100% - 120px); margin-left: auto;">
                    <button class="btn btn-primary" style="padding: 5px 10px; border: none; border-radius: 5px; background-color: #007bff; color: #fff; cursor: pointer; margin-bottom: 1.1%; width: 100px;">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdn.socket.io/4.0.1/socket.io.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.3/js/bootstrap.bundle.min.js"></script>
<script src="message.js"></script>
</body>
</html>