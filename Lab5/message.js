const socket = io('http://192.168.13.248:3000');

const form = document.getElementById('messageForm');
const input = document.getElementById('message');
const chat = document.getElementById('chat');
const userForm = document.getElementById('userForm');
const username = document.getElementById('username');
const chatArea = document.getElementById('chatArea');
const userNameArea = document.getElementById('userNameArea');
const users = document.getElementById('users');
const chatInfo = document.getElementById('chatInfo');
const notifications = document.getElementById('notifications'); // New notification area

let choosenChat = "all";
let user;
const allChats = {};
allChats["all"] = [];
let userList = [];
const newMessages = {}; // Store new message notifications

form.addEventListener('submit', (e) => {
    e.preventDefault();
    if (input.value) {
        socket.emit('send message', input.value, choosenChat);
        input.value = '';
    }
});

userForm.addEventListener('submit', (e) => {
    e.preventDefault();
    if (username.value) {
        socket.emit('new user', username.value);
        user = username.value;
        chatArea.style.display = 'flex';
        userNameArea.style.display = 'none';
    }
});

socket.on('new message', (data) => {
    if (!(data.chat in allChats)) {
        allChats[data.chat] = [];
    }
    allChats[data.chat].push(data.user + ": " + data.msg);
    renderChat();

    // If the message is not in the currently chosen chat, show a notification
    if (data.chat !== choosenChat) {
        newMessages[data.chat] = true;
        updateNotifications();
    }
});

socket.on('get user', (data) => {
    userList = data; // Store the list of users
    let html = '<div class="list-group-item user-item mt-2" data-username="all">all<div class="notification-dot"></div></div>';
    for (let i = 0; i < data.length; i++) {
        if (data[i] !== user) {
            html += `<div class="list-group-item user-item mt-2" data-username="${data[i]}">${data[i]}<div class="notification-dot"></div></div>`;
        }
    }
    users.innerHTML = html;
    updateChatInfo();
});

socket.on('new notification', (data) => {
    // Find the user item and add a new-message class to show the notification dot
    const userItem = document.querySelector(`.user-item[data-username="${data.user}"]`);
    if (userItem) {
        newMessages[data.user] = true;
        updateNotifications();
    }
});

users.addEventListener('click', (e) => {
    const userItem = e.target.closest('.user-item');
    if (userItem) {
        chooseChat(userItem);
    }
});

function chooseChat(item) {
    choosenChat = item.getAttribute('data-username');
    updateChatInfo();
    renderChat();
    // Clear the new message notification for the chosen chat
    if (newMessages[choosenChat]) {
        delete newMessages[choosenChat];
        updateNotifications();
    }
}

function updateChatInfo() {
    if (choosenChat === "all") {
        chatInfo.innerHTML = "Chat: " + userList.join(", ");
    } else {
        chatInfo.innerHTML = "Chat: " + choosenChat;
    }
}

function updateNotifications() {
    document.querySelectorAll('.user-item').forEach(item => {
        const username = item.getAttribute('data-username');
        if (newMessages[username]) {
            item.classList.add('new-message');
        } else {
            item.classList.remove('new-message');
        }
    });
}

function renderChat() {
    let html = '';
    for (let i = 0; i < allChats[choosenChat]?.length; i++) {
        const message = allChats[choosenChat][i];
        const sender = message.split(':')[0].trim(); // Extract the sender's name
        const messageContent = message.split(':').slice(1).join(':').trim(); // Extract the message content
        if (sender === user) { // Align right for the user's messages
            html += `<div style="text-align: right;"><strong>${sender}:</strong> ${messageContent}</div>`;
        } else { // Align left for other messages
            html += `<div style="text-align: left;"><strong>${sender}:</strong> ${messageContent}</div>`;
        }
    }
    chat.innerHTML = html;
    chat.scrollTop = chat.scrollHeight;
}

// Initial call to update chat info for the default selected chat
updateChatInfo();