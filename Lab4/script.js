let Student = function () {
    this.id = null;
    this.group = null;
    this.firstName = "";
    this.secondName = "";
    this.gender = null;
    this.birthday = "";
    this.status = false;
}

let group;
let gender;
let button;

const groupMappings = {
    '1': 'PZ-21',
    '2': 'PZ-22',
    '3': 'PZ-23',
    '4': 'PZ-24'
};

const genderMappings = {
    '1': 'Male',
    '2': 'Female',
    '3': 'Other'
};

window.addEventListener('load', async () => {
    if ('serviceWorker' in navigator) {
        try {
            const reg = await navigator.serviceWorker.register('sw.js')
            console.log('Service worker register success', reg)
        } catch (e) {
            console.log('Service worker register fail')
        }
    }
})

document.addEventListener("DOMContentLoaded", function() {
    const tableBody = document.querySelector('tbody');

    document.querySelector('#myForm button[type="submit"]').addEventListener('click', function(event) {
        event.preventDefault();

        let student = newStudent();
        console.log(student);

        $.ajax({
            url: "validation.php",
            type: "POST",
            data: student,
            dataType: "json",
            success: function(data) {
                clearValidation();
                console.log(data);
                if (data.status) {
                    console.log("Student:", data.user);
                    if (data.edit) {
                        editStudent(data.user);
                    } else {
                        addStudent(data.user);
                    }
                    document.getElementById('modalContainer').style.display = 'none';
                } else {
                    console.log("An error occurred:");
                    console.log("Error code:", data.error.code);
                    console.log("Error message:", data.error.message);
                    document.getElementById(data.error.type).classList.add("is-invalid");
                }
            },
            error: function(xhr, status, error) {
                console.log("An error occurred while sending the request:", error);
            }
        });
    });

    document.querySelector('.content').addEventListener('click', function (event){
        if(event.target.closest('div')?.classList.contains('button-add-edit')){
            button = event.target.closest('div');
            addOrEdit();
        }
        if(event.target.closest('div')?.classList.contains('button-delete')){
            button = event.target.closest('div');
            addDeleteEvent();
        }
    });

    // Додаємо обробник подій для закриття модального вікна при кліку на кнопку "хрестик"
    const closeModal = document.getElementById('closeModal');
    closeModal.addEventListener('click', function() {

        // Закриваємо модальне вікно
        document.getElementById('modalContainer').style.display = 'none';
    });
});

function newStudent(){
    let student = new Student();

    const firstname = document.getElementById('firstName').value.trim();
    const secondName = document.getElementById('lastName').value.trim();

    student.firstName = firstname;
    student.secondName = secondName;
    group = document.getElementById('group').value;
    gender = document.getElementById('gender').value;
    student.group = group;
    student.gender = gender;
    student.birthday = document.getElementById('birthday').value.trim();
    student.status = document.getElementById('status').checked;
    student.id = document.getElementById('rowId').value;

    return student;
}

function editStudent(student){
    console.log(student.id);
    const row = button.closest('tr');
    const id = row.getAttribute('data-id'); // Отримуємо id рядка
    const existingRow = document.querySelector(`tr[data-id="${id}"]`);

    // Перевірка, чи існує рядок для оновлення
    if (existingRow) {
        group = groupMappings[student.group] || '';
        gender = genderMappings[student.gender] || '';
        // Оновлення даних про студента в таблиці
        existingRow.cells[1].setAttribute("data-value", student.group);
        existingRow.cells[1].innerText = group;
        existingRow.cells[2].innerText = `${student.firstName} ${student.secondName}`;
        existingRow.cells[3].setAttribute("data-value", student.gender);
        existingRow.cells[3].innerText = gender;
        existingRow.cells[4].innerText = student.birthday;
        const statusIcon = existingRow.cells[5];
        statusIcon.innerHTML = `<i class="bi bi-circle-fill circle-grey ${student.status === "true" ? 'active' : ''}"></i>`;
        console.log(statusIcon.classList);
    }
    //window.location.reload();
}

function addStudent(student){
    // Перевірка наявності tableBody
    const tableBody = document.querySelector('tbody');
    if (tableBody) {
        student.group = groupMappings[group] || '';
        student.gender = genderMappings[gender] || '';

        const newRow = document.createElement('tr');
        newRow.setAttribute('data-id', student.id); // Встановлюємо атрибут data-id
        newRow.innerHTML = `
        <th scope="row">
            <input class="form-check-input" type="checkbox" value="">
        </th>
        <td data-value="${group}">${student.group}</td>
        <td>${student.firstName} ${student.secondName}</td>
        <td data-value="${gender}">${student.gender}</td>
        <td>${student.birthday}</td>
        <td><i class="bi bi-circle-fill circle-grey ${student.status ? 'active' : ''}"></i></td>
        <td>
        <div class="button-container">
        <div class="button-add-edit">
        <i class="bi bi-pencil-square"></i>
        </div>
            <div class="button-delete">
                    <i class="bi bi-x-square button-delete"></i>
                </div>
            </div>
        </td>
        `;
        tableBody.appendChild(newRow);
    } else {
        console.error("tableBody is not found");
    }
    //window.location.reload();
}

function clearValidation(){
    let invalidElements = document.querySelectorAll('.is-invalid');

    invalidElements.forEach(element => {
        element.classList.remove('is-invalid');
    })
}

function addOrEdit(){
    let student = new Student();
    student.id = document.getElementById('rowId').value;
    if(button.getAttribute('data-id') === ''){
        // Очищаємо всі поля форми
        document.getElementById('group').value = '';
        document.getElementById('firstName').value = '';
        document.getElementById('lastName').value = '';
        document.getElementById('gender').value = '';
        document.getElementById('birthday').value = '';
        document.getElementById('status').checked = false;
        document.getElementById('rowId').value = '';

        // Показуємо модальне вікно з формою для редагування
        document.getElementById('modalContainer').style.display = 'block';
    }
    else{
        const row = button.closest('tr');
        const id = row.getAttribute('data-id'); // Отримуємо id рядка
        const group = row.cells[1].getAttribute("data-value");
        const name = row.cells[2].innerText.split(' ');
        const firstName = name[0];
        const lastName = name[1];
        let genderCode = row.cells[3].getAttribute("data-value");

        const birthday = row.cells[4].innerText.split('.').reverse().join('-');
        const status = row.cells[5].querySelector('i').classList.contains('active');

        // Встановлюємо отримані дані у відповідні поля форми для редагування
        document.getElementById('group').value = group;
        document.getElementById('firstName').value = firstName;
        document.getElementById('lastName').value = lastName;
        document.getElementById('gender').value = genderCode;
        document.getElementById('birthday').value = birthday;
        document.getElementById('status').checked = status;

        // Зберігаємо id рядка для редагування
        document.getElementById('rowId').value = id;

        // Показуємо модальне вікно з формою для редагування
        document.getElementById('modalContainer').style.display = 'block';
    }
}

function addDeleteEvent() {
    const row = button.closest('tr'); // Знаходимо найближчий рядок
    row.parentNode.removeChild(row);

    let id = button.getAttribute('data-id');

    // Створення об'єкта з даними про студента для видалення
    const studentData = {
        id: id
    };

    // Відправлення запиту DELETE на сервер для видалення студента
    fetch('delete.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(studentData)
    })
        .then(response => response.json())
        .then(data => {
            if (data.success) {
                // Якщо видалення успішне, видалити рядок з таблиці
                console.log('Студент видалений успішно:', id);
            } else {
                // Якщо видалення не вдалося, вивести повідомлення про помилку
                console.error('Помилка під час видалення студента:', data.error);
            }
        })
        .catch(error => {
            console.error('Помилка під час відправлення запиту DELETE:', error);
        });
}