<?php
require 'config.php';
header("Content-Type: application/json");

// Підключення до бази даних
$conn = new mysqli('localhost', 'root', '', 'students');
// Перевірка з'єднання
if ($conn->connect_error) {
    handle_error(500, "Connection failed: " . $conn->connect_error, 'database');
}

function handle_error($code, $message, $type) {
    $response["status"] = false;
    $response["error"]["code"] = $code;
    $response["error"]["message"] = $message;
    $response["error"]["type"] = $type;
    echo json_encode($response);
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['group'])) {
        handle_error(101, "Group field can not be empty", 'group');
    }
    if (empty($_POST['firstName'])) {
        handle_error(102, "First name field can not be empty", 'firstName');
    }
    if (empty($_POST['secondName'])) {
        handle_error(103, "Last name field can not be empty", 'lastName');
    }
    if (empty($_POST['gender'])) {
        handle_error(104, "Choose the gender from the dropdown list", 'gender');
    }
    if (empty($_POST['birthday'])) {
        handle_error(105, "Choose the date of birth from the dropdown calendar", 'birthday');
    }

    // Екранування даних
    $group = mysqli_real_escape_string($conn, $_POST['group']);
    $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
    $secondName = mysqli_real_escape_string($conn, $_POST['secondName']);
    $gender = mysqli_real_escape_string($conn, $_POST['gender']);
    $birthday = mysqli_real_escape_string($conn, $_POST['birthday']);
    $status = 0;
    if(mysqli_real_escape_string($conn, $_POST['status']) == "true"){
        $status = 1;
    }

    $id = mysqli_real_escape_string($conn, $_POST['id']);

    $stmt_check_exist = $conn->prepare("SELECT id FROM students WHERE id = ?");
    $stmt_check_exist->bind_param("i", $id);
    $stmt_check_exist->execute();
    $result_check_exist = $stmt_check_exist->get_result();

    // Якщо існує запис з таким ID, виконуємо оновлення
    if ($result_check_exist->num_rows > 0) {
        // Виконуємо оновлення існуючого запису
        $stmt_update = $conn->prepare("UPDATE students SET idGroup = ?, firstName = ?, secondName = ?, idGender = ?, birthday = ?, status = ? WHERE id = ?");
        $stmt_update->bind_param("issisii", $group, $firstName, $secondName, $gender, $birthday, $status, $id);

        if ($stmt_update->execute() === TRUE) {
            $response["user"] = $_POST;
            $response["status"] = true;
            $response["edit"] = true;
            $response["message"] = "Student updated successfully";
            echo json_encode($response);
        } else {
            handle_error(500, "Error updating student: " . $conn->error, 'database');
        }
    } else {
        // Виконуємо вставку нового запису з автоматично згенерованим id
        $stmt_insert = $conn->prepare("INSERT INTO students (idGroup, firstName, secondName, idGender, birthday, status) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt_insert->bind_param("issisi", $group, $firstName, $secondName, $gender, $birthday, $status);

        if ($stmt_insert->execute() === TRUE) {
            $id = mysqli_insert_id($conn);
            $response["user"] = $_POST;
            $response["user"]["id"] = $id;
            $response["status"] = true;
            $response["edit"] = false;
            $response["message"] = "New student inserted successfully";
            echo json_encode($response);
        } else {
            handle_error(500, "Error inserting student: " . $conn->error, 'database');
        }
    }

    if(isset($stmt_insert)) $stmt_insert->close();
    if(isset($stmt_update)) $stmt_update->close();
    if(isset($stmt_check_exist)) $stmt_check_exist->close();
    $conn->close();
    exit;
}

http_response_code(403);
echo "Requested resource is forbidden";
?>
