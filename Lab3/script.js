let Student = function () {
    this.id = null;
    this.group = 0;
    this.firstName = "";
    this.secondName = "";
    this.gender = 0;
    this.dob = "";
    this.status = false;
}

let group;
let gender;
let newID = 0;

window.addEventListener('load', async () => {
    if ('serviceWorker' in navigator) {
        try {
            const reg = await navigator.serviceWorker.register('sw.js')
            console.log('Service worker register success', reg)
        } catch (e) {
            console.log('Service worker register fail')
        }
    }
})

document.addEventListener("DOMContentLoaded", function() {
    const tableBody = document.querySelector('tbody');
    let newID = 0;

    // Додаємо подію для видалення рядка для кожної іконки "x"
    tableBody.querySelectorAll('.bi-x-square').forEach(icon => {
        addDeleteEvent(icon);
    });

    document.querySelector('#myForm button[type="submit"]').addEventListener('click', function(event) {
        event.preventDefault();

        let student = newStudent();

        $.ajax({
            url: "validation.php",
            type: "POST",
            data: student,
            dataType: "json",
            success: function(data) {
                clearValidation();
                console.log(data);
                if (data.status) {
                    console.log("Student:", data.user);
                    if (student.id) {
                        editStudent(student);
                    } else {
                        addStudent(student);
                    }

                    document.getElementById('modalContainer').style.display = 'none';
                } else {
                    console.log("An error occurred:");
                    console.log("Error code:", data.error.code);
                    console.log("Error message:", data.error.message);
                    document.getElementById(data.error.type).classList.add("is-invalid");
                }
            },
            error: function(xhr, status, error) {
                console.log("An error occurred while sending the request:", error);
            }
        });
    });

    document.querySelector('.content').addEventListener('click', function (event){
        console.log(event.target);
        if(event.target.closest('div')?.classList.contains('button-add-edit')){
            addOrEdit(event.target.closest('div'));
        }
        if(event.target.closest('div')?.classList.contains('button-delete')){
            addDeleteEvent(event.target.closest('div'));
        }
    });

    // Додаємо обробник подій для закриття модального вікна при кліку на кнопку "хрестик"
    const closeModal = document.getElementById('closeModal');
    closeModal.addEventListener('click', function() {

        // Закриваємо модальне вікно
        document.getElementById('modalContainer').style.display = 'none';
    });
});

function newStudent(){
    let student = new Student();

    const firstname = document.getElementById('firstName').value.trim();
    const secondName = document.getElementById('lastName').value.trim();
    const dob = document.getElementById('dob').value.trim();

    student.firstName = firstname;
    student.secondName = secondName;
    student.group = document.getElementById('group').value;
    student.gender = document.getElementById('gender').value;
    student.dob = formatDate(dob);
    student.status = document.getElementById('status').checked;
    student.id = document.getElementById('rowId').value;

    const groupMappings = {
        '1': 'PZ-21',
        '2': 'PZ-22',
        '3': 'PZ-23',
        '4': 'PZ-24'
    };

    const genderMappings = {
        '1': 'Male',
        '2': 'Female',
        '3': 'Other'
    };

    student.group = groupMappings[student.group] || '';
    student.gender = genderMappings[student.gender] || '';


    return student;
}

function editStudent(student){
    const existingRow = document.querySelector(`tr[data-id="${student.id}"]`);

    if (existingRow) {
        existingRow.cells[1].setAttribute("data-value", group);
        existingRow.cells[1].innerText = student.group;
        existingRow.cells[2].innerText = `${student.firstName} ${student.secondName}`;
        existingRow.cells[3].innerText = student.gender;
        existingRow.cells[3].setAttribute("data-value", gender);
        existingRow.cells[4].innerText = student.dob;
        const statusIcon = existingRow.cells[5].querySelector('i');
        statusIcon.classList.toggle('active', student.status);
    }
}

function addStudent(student){
    // Перевірка наявності tableBody
    const tableBody = document.querySelector('tbody');
    if (tableBody) {
        // Додавання нового студента, якщо student.id не існує
        const newRow = document.createElement('tr');
        newRow.setAttribute('data-id', newID); // Встановлюємо атрибут data-id
        newID++;
        newRow.innerHTML = `
        <th scope="row">
            <input class="form-check-input" type="checkbox" value="">
        </th>
        <td data-value="${group}">${student.group}</td>
        <td>${student.firstName} ${student.secondName}</td>
        <td data-value="${gender}">${student.gender}</td>
        <td>${student.dob}</td>
        <td><i class="bi bi-circle-fill circle-grey ${student.status ? 'active' : ''}"></i></td>
        <td>
        <div class="button-container">
        <div class="button-add-edit">
        <i class="bi bi-pencil-square"></i>
        </div>
            <div class="button-delete">
                    <i class="bi bi-x-square button-delete"></i>
                </div>
            </div>
        </td>
        `;
        tableBody.appendChild(newRow);
    } else {
        console.error("tableBody is not found");
    }
}

function clearValidation(){
    let invalidElements = document.querySelectorAll('.is-invalid');

    invalidElements.forEach(element => {
        element.classList.remove('is-invalid');
    })
}

// Функція для форматування дати в формат "дд.мм.рррр"
function formatDate(date) {
    const parts = date.split('-');
    return `${parts[2]}.${parts[1]}.${parts[0]}`;
}

function addOrEdit(button){
    let student = new Student();
    student.id = document.getElementById('rowId').value;
    if(button.getAttribute('data-id') === ''){
        // Очищаємо всі поля форми
        document.getElementById('group').value = '';
        document.getElementById('firstName').value = '';
        document.getElementById('lastName').value = '';
        document.getElementById('gender').value = '';
        document.getElementById('dob').value = '';
        document.getElementById('status').checked = false;
        document.getElementById('rowId').value = '';

        // Показуємо модальне вікно з формою для редагування
        document.getElementById('modalContainer').style.display = 'block';
    }
    else{
        const row = button.closest('tr');
        const id = row.getAttribute('data-id'); // Отримуємо id рядка
        const group = row.cells[1].getAttribute("data-value");
        const name = row.cells[2].innerText.split(' ');
        const firstName = name[0];
        const lastName = name[1];
        let genderCode = row.cells[3].getAttribute("data-value");

        const dob = row.cells[4].innerText.split('.').reverse().join('-');
        const status = row.cells[5].querySelector('i').classList.contains('active');

        // Встановлюємо отримані дані у відповідні поля форми для редагування
        document.getElementById('group').value = group;
        document.getElementById('firstName').value = firstName;
        document.getElementById('lastName').value = lastName;
        document.getElementById('gender').value = genderCode;
        document.getElementById('dob').value = dob;
        document.getElementById('status').checked = status;

        // Зберігаємо id рядка для редагування
        document.getElementById('rowId').value = id;

        // Показуємо модальне вікно з формою для редагування
        document.getElementById('modalContainer').style.display = 'block';
    }
}

// Функція для додавання обробника подій для іконки "x"
function addDeleteEvent(button) {
    const row = button.closest('tr'); // Знаходимо найближчий рядок
    row.parentNode.removeChild(row);
}