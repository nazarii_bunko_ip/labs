<?php
require 'config.php';
header("Content-Type: application/json");

function handle_error($code, $message, $type) {
    $response["status"] = false;
    $response["error"]["code"] = $code;
    $response["error"]["message"] = $message;
    $response["error"]["type"] = $type;
    echo json_encode($response);
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['group']) && isset($arrGroup[$_POST['group']])) {
        handle_error(101, "Choose the group from the dropdown list", 'group');
    }
    if (empty($_POST['firstName'])) {
        handle_error(102, "First name field can not be empty", 'firstName');
    }
    if (empty($_POST['secondName'])) {
        handle_error(103, "Last name field can not be empty", 'lastName');
    }
    if (empty($_POST['gender']) && isset($arrGroup[$_POST['gender']])) {
        handle_error(104, "Choose the gender from the dropdown list", 'gender');
    }
    if (empty($_POST['dob']) || strtotime($_POST['dob']) === false) {
        handle_error(105, "Choose the date of birth from the dropdown calendar", 'dob');
    }

    $response["status"] = true;
    $response["user"] = $_POST;
    echo json_encode($response);
    exit;
}

http_response_code(403);
echo "Requested resource is forbidden";

