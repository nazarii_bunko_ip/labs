<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="manifest" href="manifest.json">
  <title>Students</title>
</head>
<body>
<?php include("config.php"); ?>
<div class="pos-f-t">
  <div class="offcanvas offcanvas-start bg-dark" tabindex="-1" id="navbarToggleExternalContent" aria-labelledby="navbarToggleExternalContentLabel">
    <div class="offcanvas-header">
      <h5 class="text-white" id="navbarToggleExternalContentLabel">Menu</h5>
      <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link text-white" href="dashboards.html">Dashboards</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white active" href="#">Students</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="tasks.html">Tasks</a>
        </li>
      </ul>
    </div>
  </div>
  <nav class="navbar navbar-dark bg-secondary">
    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="d-flex">
      <div class="bell-div">
        <a href="messages.html" class="text-black">
          <i class="bi bi-bell"></i>
        </a>
        <div class="blink"></div>
      </div>
      <div class="logo-div">
        <img src="Greendenvald.jpg" alt="Logo" class="logo">
        <div class="logo-menu">
          <a href="#">Profile</a>
          <a href="#">Log Out</a>
        </div>
      </div>
    </div>
  </nav>
</div>
<div class="line">
  <p id="CMS">CMS</p>
  <div class="d-flex bell_logo">
    <div class="bell-div">
      <a href="messages.html" class="text-black">
        <i class="bi bi-bell"></i>
      </a>
      <div class="blink"></div>
      <div class="bell-menu">
        <a>
          <div class="message-div">
            <img src="Avatar1.jpg" alt="Avatar" class="message-avatar">
            <div class="triangle-container">
              <div class="triangle-border"></div>
            </div>
            <div class="message-message">
              <span>Привіт, як ти?</span>
            </div>
          </div>
        </a>
        <a>
          <div class="message-div">
            <img src="Avatar2.jpg" alt="Avatar" class="message-avatar">
            <div class="triangle-container">
              <div class="triangle-border"></div>
            </div>
            <div class="message-message">
              <span>Здороууу</span>
            </div>
          </div>
        </a>
        <a>
          <div class="message-div">
            <img src="Avatar3.jpeg" alt="Avatar" class="message-avatar">
            <div class="triangle-container">
              <div class="triangle-border"></div>
            </div>
            <div class="message-message">
              <span>Ти зробив ПВІ?</span>
            </div>
          </div>
        </a>
      </div>
    </div>
    <div class="logo-div">
      <img src="Greendenvald.jpg" alt="Logo" class="logo">
    </div>
    <div class="fs-2 logo-text">ItsNeMo
      <div class="logo-menu">
        <a href="#">Profile</a>
        <a href="#">Log Out</a>
      </div>
    </div>
  </div>
</div>

<div class="dropdown">
  <a href="dashboards.html" class="list-group-item list-group-item-action border-0 mb-2 mt-3">Dashboard</a>
  <a href="#" class="list-group-item list-group-item-action border-0 mb-2 active">Students</a>
  <a href="tasks.html" class="list-group-item list-group-item-action border-0 mb-2">Tasks</a>
</div>

<div id="String">Students</div>

<div class="modal-container" id="modalContainer">
  <div class="modal-content">
    <span class="close" id="closeModal">&times;</span>
    <form id="myForm">
      <input type="hidden" id="rowId">
      <div class="form-group">
        <label for="group">Group</label>
        <select class="form-control" id="group">
          <option selected disabled value="">Choose Group</option>
          <?php foreach($arrGroup as $key => $group){ ?>
          <option value ="<?= $key ?>"><?= $group ?></option>
          <?php } ?>
        </select>
          <div class="invalid-feedback">Choose Group</div>
      </div>
      <div class="form-group">
        <label for="firstName">First Name</label>
        <input type="text" class="form-control" id="firstName" placeholder="Enter first name">
          <div class="invalid-feedback">Enter First Name</div>
      </div>
      <div class="form-group">
        <label for="lastName">Last Name</label>
        <input type="text" class="form-control" id="lastName" placeholder="Enter last name">
          <div class="invalid-feedback">Enter Last Name</div>
      </div>
      <div class="form-group">
        <label for="gender">Gender</label>
        <select class="form-control" id="gender">
            <option selected disabled value="">Choose Gender</option>
            <?php foreach($arrGender as $key => $gender){ ?>
                <option value ="<?= $key ?>"><?= $gender ?></option>
            <?php } ?>
        </select>
          <div class="invalid-feedback">Choose Gender</div>
      </div>
      <div class="form-group">
        <label for="dob">Date of Birth</label>
        <input type="date" class="form-control" id="dob">
          <div class="invalid-feedback">Choose Day of Birth</div>
      </div>
      <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="status">
        <label class="form-check-label" for="status">Active</label>
      </div>
      <button class="btn btn-primary" type="submit">Submit</button>
    </form>
  </div>
</div>

<div class="content">
  <div class="button-add-edit plus-square" data-id="">
    <i class="bi bi-plus-square"></i>
  </div>

  <div class="table-responsive" id="table-div">
    <table class="table table-striped">
      <thead>
      <tr>
        <th scope="col"><i class="bi bi-square"></i></th>
        <th scope="col">Group</th>
        <th scope="col">Name</th>
        <th scope="col">Gender</th>
        <th scope="col">Birthday</th>
        <th scope="col">Status</th>
        <th scope="col">Options</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>


<ul class="pagination page-navigation">
  <li class="page-item"><a class="page-link transparent" href="#"><</a></li>
  <li class="page-item"><a class="page-link transparent" href="#">1</a></li>
  <li class="page-item"><a class="page-link transparent" href="#">2</a></li>
  <li class="page-item"><a class="page-link transparent" href="#">3</a></li>
  <li class="page-item"><a class="page-link transparent" href="#">></a></li>
</ul>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.3/js/bootstrap.bundle.min.js"></script>
<script src="script.js"></script>
</body>
</html>