document.addEventListener("DOMContentLoaded", function() {
    const logoImg = document.querySelector('.logo');
    const logoMenu = document.querySelector('.logo-menu');

    logoImg.addEventListener('click', function(event) {
        event.stopPropagation(); // Зупинити спливання події
        logoMenu.classList.toggle('active');
    });

    // Закривати меню при кліку поза ним
    document.addEventListener('click', function() {
        logoMenu.classList.remove('active');
    });

    logoMenu.addEventListener('click', function(event) {
        event.stopPropagation(); // Зупинити спливання події
    });
    const tableBody = document.querySelector('tbody');

    // Функція для видалення рядка таблиці
    function deleteRow(row) {
        row.parentNode.removeChild(row);
    }

    // Функція для додавання обробника подій для іконки "x"
    function addDeleteEvent(icon) {
        icon.addEventListener('click', function() {
            const row = this.closest('tr'); // Знаходимо найближчий рядок
            deleteRow(row); // Видаляємо рядок
        });
    }

    // Додаємо подію для видалення рядка для кожної іконки "x"
    tableBody.querySelectorAll('.bi-x-square').forEach(icon => {
        addDeleteEvent(icon);
    });

    // Додаємо подію для додавання нового рядка
    const addButton = document.querySelector('.bi-plus-square');
    addButton.addEventListener('click', function() {
        const newRow = document.createElement('tr');
        newRow.innerHTML = `
            <th scope="row">
                <input class="form-check-input" type="checkbox" value="">
            </th>
            <td>New Group</td>
            <td>New Name</td>
            <td>New Gender</td>
            <td>New Birthday</td>
            <td><i class="bi bi-circle-fill circle-grey"></i></td>
            <td>
                <i class="bi bi-pencil-square"></i>
                <i class="bi bi-x-square"></i>
            </td>
        `;
        tableBody.appendChild(newRow);

        // Додаємо обробник подій для новоствореної іконки "x"
        const deleteIcon = newRow.querySelector('.bi-x-square');
        addDeleteEvent(deleteIcon);
    });
});

